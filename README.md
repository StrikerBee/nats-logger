## Very simple NATS client example

This is a very simple rust program that subs to all topics at a NATS server. (Using ">")

Only commandline arg is the host you want to use. If none is provided, the demo nats server ("demo.nats.io") is used.



Usage
```
cargo run <host> 
or 
.nats-logger-rust <host>

or log to file, and redirect stderr to stdout

./nats-logger-rust <host> >file.log 2>&1 
```
