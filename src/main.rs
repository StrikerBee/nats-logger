use nats;
use chrono::{DateTime, Utc};
use std::env;


fn main() {
    let args: Vec<String> = env::args().collect();
    
    //Default nats testing host
    let mut host = "demo.nats.io";
    println!("NATS logger");
    // Get arg. TODO Validate url.
    if args.len() == 2 {
        host = &args.get(1).unwrap();
    } 
    
    println!("Connection to NATS host at {:?}",&host);
    let nc = nats::connect(host).unwrap();
    
    // Subscribing on ">" to sub on all message topics.
    let sub = nc.subscribe(">").unwrap();
    for msg in sub.messages() {
        let now: DateTime<Utc> = Utc::now();
        println!("{},{:?},{:?},{:?}", now.to_rfc2822(),msg.subject,msg.headers,msg.reply);
    }
}
